package com.tm.demo.web;

import com.tm.demo.domain.Product;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {


    @RequestMapping("/{userId}")
    public String displayUser(@PathVariable int userId) {
        return "User found: " + userId;
    }

    @RequestMapping("/{userId}/invoices")
    public String displayUserInvoices(@PathVariable int userId, @RequestParam(value = "date", required = false) Date date) {
        return String.format("Invoice found for user: %s, on %s date", userId, date);
    }

    @RequestMapping("/{userId}/items")
    public List<String> displayStringJson() {
        return Arrays.asList("Shoes", "laptop", "button");
    }

    @RequestMapping("/{userId}/products")
    public List<Product> displayProductsJson() {
        return Arrays.asList(new Product(1, "shoes", 432.11),
                new Product(2, "books", 12),
                new Product(3, "bag", 32));
    }
}
