package com.tm.demo.web;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/greeting")
public class HelloController {

    @RequestMapping("/basic")
    public String sayHello() {
        return "<h1>hello</h1>";
    }

    @RequestMapping("/proper")
    public String sayProperHello() {
        return "<h1>hello there, how are you?</h1>";
    }

    @RequestMapping("user_entry")
    public String userForm() {
        return "<form action=\"/greeting/user_greeting\" method=\"POST\">\n" +
                "  <label for=\"fname\">First name:</label><br>\n" +
                "  <input type=\"text\" id=\"fname\" name=\"fname\"><br>\n" +
                "  <label for=\"lname\">Last name:</label><br>\n" +
                "  <input type=\"text\" id=\"lname\" name=\"lname\"><br><br>\n" +
                "  <input type=\"submit\" value=\"Submit\">\n" +
                "</form>";

    }

    @RequestMapping(value = "user_greeting", method = RequestMethod.POST)
    public String userGreeting(@RequestParam("fname") final String firstName, @RequestParam("lname") final String lastName) {
        return String.format("Hello %s, %s", firstName, lastName);
    }

    @RequestMapping(value = "/order/{id}", method = RequestMethod.GET)
    public String getOrder(@PathVariable final String id) {
        return String.format("Order number: %s", id);
    }
}
